#! /bin/sh
set -e
BIN="$0"

test_and_run () {
	local flag="$1"
	if grep -q "$flag" /proc/cpuinfo && [ -x "$BIN.$flag" ]
	then
		shift
		exec "$BIN.$flag" "$@"
	fi
}

for SIMDE in avx2 avx sse4.1 ssse3 sse3 sse2 sse
do test_and_run "$SIMDE" "$@"
done

# fallback to plain option
exec "$BIN.generic" "$@"
